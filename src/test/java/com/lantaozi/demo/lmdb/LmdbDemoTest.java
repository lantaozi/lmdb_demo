package com.lantaozi.demo.lmdb;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class LmdbDemoTest {

  @Test
  public void CheckDate() {
    Assert.assertTrue(LocalDate.now().getDayOfWeek().getValue() < 10);
  }
}
