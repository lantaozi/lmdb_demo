package com.lantaozi.demo.lmdb;

import caffe.Caffe;
import org.lmdbjava.*;
import org.lmdbjava.Cursor;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.nio.ByteBuffer;

import static java.nio.charset.StandardCharsets.UTF_8;

public class LmdbDemo {

  public static void main(String[] args) throws IOException {
    final String filePath = "/Users/lantaozi/Developments/Labs/lmdb_demo/src/main/resources/lmdb_tmp";
    final File dbFile = new File(filePath);

    final Env<ByteBuffer> env = Env.create().open(dbFile);

    System.out.println(env.stat());
    System.out.println(env.info());

    final Dbi<ByteBuffer> dbi = env.openDbi((byte[]) null);
    try (Txn<ByteBuffer> txn = env.txnRead();
         Cursor<ByteBuffer> cursor = dbi.openCursor(txn)) {
      if (cursor.first()) {
        do {
          final ByteBuffer key = cursor.key();
          final ByteBuffer val = cursor.val();

          final String imageFileName = UTF_8.decode(key).toString();
          System.out.println("key = " + imageFileName);
          // System.out.println("val = " + UTF_8.decode(val).toString());

          final byte[] vals = new byte[val.capacity()];
          val.get(vals, 0, val.capacity());

          Caffe.Datum datum = Caffe.Datum.parseFrom(vals);

          System.out.println(datum.getLabel());

          System.out.println(datum.getChannels());
          System.out.println(datum.getHeight());
          System.out.println(datum.getWidth());


          /**
           * def datum_to_array(datum):
           *     """Converts a datum to an array. Note that the label is not returned,
           *     as one can easily get it by calling datum.label.
           *     """
           *     if len(datum.data):
           *         return np.fromstring(datum.data, dtype=np.uint8).reshape(
           *             datum.channels, datum.height, datum.width)
           *     else:
           *         return np.array(datum.float_data).astype(float).reshape(
           *             datum.channels, datum.height, datum.width)
           */
          /**
           * image = np.transpose(data, (1, 2, 0))
           */



        } while (cursor.next());
      }
    }
  }
}
